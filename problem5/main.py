print("Enter the start position for deletion: ")
startIterator = int(input())

print("Enter the last position for deletion: ")
endIterator = int(input())

fin = open("data.txt", "rt")
fout = open("data1.txt", "wt")

line = fin.readline()

if(len(line) < endIterator):
	print("Length of list isn't enough.")
else:
	line = line[0: startIterator:] + line[endIterator + 1::]

fout.write(line)
